const express = require("express");
const app = express();
const dotenv = require("dotenv");
const moment = require("moment");
const cors = require("cors");
const data = require("./data/data.js");
dotenv.config();
const port = process.env.PORT;
app.use(cors());
app.get("/all-states", (req, res) => {
  let uniqueState = [];
  data.forEach((saleData) => {
    if (!uniqueState.includes(saleData.State)) {
      uniqueState.push(saleData.State);
    }
  });
  uniqueState.sort();
  res.send(200, uniqueState);
});
app.get("/min-max-dates", (req, res) => {
  const filteredData = data.filter((item) => item.State === req.query.state);
  const orderDates = data.map((item) =>
    moment(item["Order Date"], "YYYY-MM-DD")
  );
  const minOrderDate = moment.min(orderDates).format("YYYY-MM-DD");
  const maxOrderDate = moment.max(orderDates).format("YYYY-MM-DD");
  res.send(200, [minOrderDate, maxOrderDate]);
});
app.get("/dashboard", (req, res) => {
  const filteredData = data.filter((item) => {
    if (item.State !== req.query.state) return false;

    const orderDate = moment(item["Order Date"], "YYYY-MM-DD");
    const fromDate = req.query.fromDate
      ? moment(req.query.fromDate, "YYYY-MM-DD")
      : null;
    const toDate = req.query.toDate
      ? moment(req.query.toDate, "YYYY-MM-DD")
      : null;

    // Check if order date is within the specified date range
    if (fromDate && orderDate.isBefore(fromDate, "day")) return false;
    if (toDate && orderDate.isAfter(toDate, "day")) return false;

    return true;
  });

  let totalData = {
    totalSale: 0,
    totalQuantity: 0,
    totalProfit: 0,
    totalDiscount: 0,
    percentage: 0,
  };
  let saleByCity = {};
  let saleByProductName = {};
  let saleByCategory = {};
  let saleBySubCategory = {};
  let saleBySegment = {};
  filteredData.forEach((item, key) => {
    totalData.totalSale += item.Sales;
    totalData.totalQuantity += item.Quantity;
    totalData.totalProfit += item.Profit;
    totalData.totalDiscount += item.Discount;
    if (saleByCity[item.City]) {
      saleByCity[item.City] += item.Sales;
    } else {
      saleByCity[item.City] = item.Sales;
    }
    let productnName = item["Product Name"];
    if (saleByProductName[productnName]) {
      saleByProductName[productnName] += item.Sales;
    } else {
      saleByProductName[productnName] = item.Sales;
    }
    let categoryName = item["Category"];
    if (saleByCategory[categoryName]) {
      saleByCategory[categoryName] += item.Sales;
    } else {
      saleByCategory[categoryName] = item.Sales;
    }
    let subCategoryName = item["Sub-Category"];
    if (saleBySubCategory[subCategoryName]) {
      saleBySubCategory[subCategoryName] += item.Sales;
    } else {
      saleBySubCategory[subCategoryName] = item.Sales;
    }
    let segmentName = item["Segment"];
    if (saleBySegment[segmentName]) {
      saleBySegment[segmentName] += item.Sales;
    } else {
      saleBySegment[segmentName] = item.Sales;
    }
  });
  console.log("saleBySegment :>> ", saleBySegment);
  res.send(200, {
    totalData,
    saleByCity,
    saleByProductName,
    saleByCategory,
    saleBySubCategory,
    saleBySegment,
  });
});
app.get("/chart", (req, res) => {});
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
