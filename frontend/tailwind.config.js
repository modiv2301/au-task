/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    screens: {
      sm: '575px',
      md: '768px',
      lg: '991px',
      xl: '1199px',
      '2xl': '1330px'
    },
    extend: {
      colors: {
        DarkBlue: '#191825',
        LightBlue: '#102c79',
        SkyBlue: '#63C3EB',
        gradpurple: '#102c79',
        gradblue: '#191825',
        headerBg:"#0c6470",
        sidebarColor:"#4a4a4a"
      }
    },
    variants: {
      extend: {
        display: ['group-focus']
      }
    }
  },
  plugins: []
}
//FF6464
//102c79