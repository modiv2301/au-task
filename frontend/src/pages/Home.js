import React, { useEffect, useState } from "react";
import SalesCity from "../components/HomeComponents/SalesCity";
import SalesByProduct from "../components/HomeComponents/SalesByProduct";
import SalesByCategory from "../components/HomeComponents/SalesByCategory";
import SalesBySubCategory from "../components/HomeComponents/SalesBySubCategory";
import SalesBySegment from "../components/HomeComponents/SalesBySegment";
import axios from "axios";
import moment from "moment";
const Home = () => {
  const [states, setStates] = useState([]);
  const [minMaxDates, setMinMaxDates] = useState([]);
  const [selectedState, setSelectedState] = useState([]);
  const [dashboard, setDashboard] = useState({});
  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");
  useEffect(() => {
    fetchStates();
  }, []);
  useEffect(() => {
    fetchMinMaxDates(selectedState);
    fetchDashboard(selectedState);
  }, [selectedState, fromDate, toDate]);
  const fetchStates = async () => {
    let statesList = await axios.get(
      process.env.REACT_APP_API_URL + "/all-states"
    );
    setStates(statesList.data);
    setSelectedState(statesList.data[0]);
    await fetchMinMaxDates(statesList.data[0]);
    await fetchDashboard(statesList.data[0]);
  };
  const fetchMinMaxDates = async (selectedState) => {
    let minMax = await axios.get(
      process.env.REACT_APP_API_URL + "/min-max-dates?state=" + selectedState
    );
    setMinMaxDates(minMax.data);
  };
  const fetchDashboard = async (selectedState) => {
    let dashboardData = await axios.get(
      process.env.REACT_APP_API_URL +
        "/dashboard?state=" +
        selectedState +
        "&fromDate=" +
        fromDate +
        "&toDate=" +
        toDate
    );
    setDashboard(dashboardData.data);
  };
  const formatUSD = (amount) => {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(amount);
  };
  console.log("dashboard :>> ", dashboard);
  return (
    <div>
      <div className="mb-10 flex items-center justify-between">
        <h4 className="text-2xl">
          <strong>Sales Overview</strong>
        </h4>
        <form className="flex items-center">
          <div className="form-group mr-5 w-[150px]">
            <label className="block font-bold mb-2">Select a state</label>
            <select
              className="w-full py-2 px-2"
              onChange={(e) => setSelectedState(e.target.value)}
            >
              {states.map((state, index) => (
                <option key={index} value={state}>
                  {state}
                </option>
              ))}
            </select>
          </div>
          <div className="form-group mr-5 w-[150px]">
            <label className="block font-bold mb-2">Select From date</label>
            <input
              className="w-full py-2 px-2"
              type="date"
              onChange={(e) => setFromDate(e.target.value)}
              value={fromDate}
              min={minMaxDates.length ? minMaxDates[0] : ""}
              max={minMaxDates.length ? minMaxDates[1] : ""}
            />
          </div>

          <div className="form-group mr-5 w-[150px]">
            <label className="block font-bold mb-2">Select To date</label>
            <input
              className="w-full py-2 px-2"
              type="date"
              onChange={(e) => setToDate(e.target.value)}
              value={toDate}
              min={minMaxDates.length ? minMaxDates[0] : ""}
              max={minMaxDates.length ? minMaxDates[1] : ""}
            />
          </div>
        </form>
      </div>

      <div className="grid grid-cols-4 gap-x-8 mb-5">
        <div className="bg-white p-4 flex items-center">
          <figure className="mr-6">
            <img src="./images/icons/icon1.png" alt="" />
          </figure>
          <figcaption>
            <span className="block text-lg mb-1 font-semibold	">
              Total Sales
            </span>
            <strong className="block text-3xl">
              {formatUSD(dashboard?.totalData?.totalSale)}
            </strong>
          </figcaption>
        </div>
        <div className="bg-white p-4 flex items-center">
          <figure className="mr-6">
            <img src="./images/icons/icon2.png" alt="" />
          </figure>
          <figcaption>
            <span className="block text-lg mb-1 font-semibold	">
              Quantity Sold
            </span>
            <strong className="block text-3xl">
              {dashboard?.totalData?.totalQuantity}
            </strong>
          </figcaption>
        </div>
        <div className="bg-white p-4 flex items-center">
          <figure className="mr-6">
            <img src="./images/icons/icon3.png" alt="" />
          </figure>
          <figcaption>
            <span className="block text-lg mb-1 font-semibold	">Discount%</span>
            <strong className="block text-3xl">55.6%</strong>
          </figcaption>
        </div>
        <div className="bg-white p-4 flex items-center">
          <figure className="mr-6">
            <img src="./images/icons/icon4.png" alt="" />
          </figure>
          <figcaption>
            <span className="block text-lg mb-1 font-semibold	">Profit</span>
            <strong className="block text-3xl">
              {formatUSD(dashboard?.totalData?.totalProfit)}
            </strong>
          </figcaption>
        </div>
      </div>

      <div className="grid grid-cols-2 gap-x-8">
        <SalesCity data={dashboard?.saleByCity ? dashboard?.saleByCity : {}} />
        <SalesByProduct
          data={
            dashboard?.saleByProductName ? dashboard?.saleByProductName : {}
          }
        />
      </div>
      <div className="grid grid-cols-3 gap-x-8">
        <SalesByCategory
          subject="Category"
          data={dashboard?.saleByCategory ? dashboard?.saleByCategory : {}}
        />
        <SalesBySubCategory
          data={
            dashboard?.saleBySubCategory ? dashboard?.saleBySubCategory : {}
          }
        />
        <SalesByCategory
          subject="Segment"
          data={dashboard?.saleBySegment ? dashboard?.saleBySegment : {}}
        />
      </div>
    </div>
  );
};

export default Home;
