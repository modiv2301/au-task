import React, { useState } from 'react';
import { Outlet } from 'react-router-dom';
import Header from './Header';
import Sidebar from './Sidebar';

const Layout = () => {

  const [isOpen,setIsOpen]=useState(false);

  const handleToggle = () =>{
    setIsOpen(!isOpen)
  }

  return (
    <div>
        <Header handleToggle={handleToggle} />
      <main className='flex'>
        <Sidebar isOpen={isOpen}/>
       <div className={`right_panel  w-[calc(100%-219px)] ml-[219px] p-5 mt-20 ${isOpen ? "openMain":""}`}>
       <Outlet />
       </div>
      </main>
    </div>
  );
};

export default Layout;
