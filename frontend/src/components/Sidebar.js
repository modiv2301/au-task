import React from 'react'
import { Link } from 'react-router-dom'
import sidebarMenu from '../utils/constant'

const Sidebar = ({isOpen}) => {
  return (
    <div className={`sidebar bg-sidebarColor fixed h-full overflow-y-auto w-[219px] pt-20 ${isOpen ? "open" : ""}`}>
       <ul className='pt-4'>
        {
            sidebarMenu?.map((menu,i)=>{
                return <li key={i} ><Link to="/" className='text-white px-4  text-sm flex items-center py-2 mb-7'> <img src={menu?.icon} alt='' className='mr-3'/> {menu?.menu}</Link></li>
            })
        }
       </ul>
    </div>
  )
}

export default Sidebar