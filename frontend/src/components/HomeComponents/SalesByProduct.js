import React, { useEffect, useState } from "react";

const SalesByProduct = ({ data = {} }) => {
  const formatUSD = (amount) => {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(amount);
  };
  const [saleData, setSaleData] = useState([]);

  useEffect(() => {
    const entries = Object.entries(data).slice(0, 10);
    setSaleData(entries);
  }, [data]);

  return (
    <div className="bg-white px-7 pb-10">
      <h4 className="font-bold text-lg ml-0 mt-5 mb-7">Sales by Products</h4>

      <div>
        <h6 className="flex justify-between mb-5">
          <strong>Product Name</strong>{" "}
          <strong className="mr-20">Sales in $</strong>
        </h6>
      </div>

      <ul className="">
        {saleData.map(([productName, sales], i) => {
          return (
            <li
              key={i}
              className="flex justify-between px-4 bg-[#D6EFF3] mb-3 items-center pr-0"
            >
              {productName}
              <p className="mb-0 bg-[#8BD0E0] py-2 min-w-[143px] text-end pr-5 font-bold">
                {formatUSD(sales)}
              </p>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default SalesByProduct;
