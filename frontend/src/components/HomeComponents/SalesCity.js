import React, { useEffect, useState } from "react";
import ReactApexChart from "react-apexcharts";

const SalesCity = ({ data }) => {
  const [citys, setCitys] = useState([]);
  const [citysValue, setCitysValue] = useState([]);
  useEffect(() => {
    const entries = Object.entries(data).slice(0, 10);
    console.log("entries :>> ", data);
    const citysList = entries.map(([key]) => key);
    const dataList = entries.map(([_, value]) => parseFloat(value).toFixed(2));
    setCitys(citysList);
    setCitysValue(dataList);
  }, [data]);

  const series = [
    {
      data: citysValue,
    },
  ];

  const options = {
    chart: {
      type: "bar",
      height: 500,
    },
    plotOptions: {
      bar: {
        borderRadius: 0,
        horizontal: true,
      },
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      categories: citys,
      labels: {
        trim: false, // Trim x-axis labels if they exceed the available space
      },
    },
    yaxis: {
      labels: {
        maxWidth: 100, // Adjust based on your chart width and label length
      },
    },
  };

  return (
    <div className="bg-white">
      <h4 className="font-bold text-lg ml-10 mt-5">Sales by City</h4>
      <ReactApexChart
        options={options}
        series={series}
        type="bar"
        height={350}
      />
    </div>
  );
};

export default SalesCity;
