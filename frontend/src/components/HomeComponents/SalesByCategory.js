import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

const SalesByCategory = ({ data, subject }) => {
  const [category, setCategory] = useState([]);
  const [categoryList, setCategoryList] = useState([]);
  useEffect(() => {
    const entries = Object.entries(data);
    const categories = entries.map(([key]) => key);
    const dataList = entries.map(([_, value]) => parseInt(value));
    setCategory(categories);
    setCategoryList(dataList);
  }, [data]);
  console.log("setCategory :>> ", categoryList);
  // Example data for series and colors
  const series = categoryList;
  const colors = ["#008FFB", "#00E396", "#FEB019", "#FF4560", "#775DD0"];
  const labels = category;

  const options = {
    chart: {
      type: "donut",
      height: 800,
    },
    plotOptions: {
      pie: {
        donut: {
          labels: {
            show: true,
          },
          size: "65%",
        },
      },
    },
    dataLabels: {
      enabled: false,
    },
    labels: labels,
    colors: colors,
    responsive: [
      {
        breakpoint: 1400,
        options: {
          plotOptions: {
            bar: {
              horizontal: false,
            },
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  return (
    <div className="bg-white px-7 pb-10  mt-10">
      <h4 className="font-bold text-lg ml-0 mt-5 mb-7">Sales By {subject}</h4>
      <div className="pie-chart-thread">
        <Chart options={options} series={series} type="donut" height="300px" />
      </div>
    </div>
  );
};

export default SalesByCategory;
