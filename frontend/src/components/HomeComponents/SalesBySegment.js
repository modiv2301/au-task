import React from "react";
import Chart from "react-apexcharts";

const SalesBySegment = () => {
  // Example data for series and colors
  const series = [44, 55, 13, 43, 22];
  const colors = ["#008FFB", "#00E396", "#FEB019", "#FF4560", "#775DD0"];
  const labels = ["Team A", "Team B", "Team C", "Team D", "Team E"];

  const options = {
    chart: {
      type: "donut",
    },
    plotOptions: {
      pie: {
        donut: {
          labels: {
            show: true,
          },
          size: "65%",
        },
      },
    },
    dataLabels: {
      enabled: false,
    },
    labels: labels,
    colors: colors,
    responsive: [
      {
        breakpoint: 1400,
        options: {
          plotOptions: {
            bar: {
              horizontal: false,
            },
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  return (
    <div className="bg-white px-7 pb-10  mt-10">
      <h4 className="font-bold text-lg ml-0 mt-5 mb-7">Sales By Segment</h4>
      <div className="pie-chart-thread">
        <Chart options={options} series={series} type="donut" height="300px" />
      </div>
    </div>
  );
};

export default SalesBySegment;
