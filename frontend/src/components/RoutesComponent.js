import React from 'react';
import { useRoutes } from 'react-router-dom';

import Home from '../pages/Home';
import Layout from './Layout';

const RoutesComponent = () => {
  const routes = useRoutes([
    {
      path: '/',
      element: <Layout />,
      children: [
        { path: '/', element: <Home /> },
      ],
    },
  ]);

  return routes;
};

export default RoutesComponent;
