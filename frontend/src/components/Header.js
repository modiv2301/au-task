import React from 'react'

const Header = ({handleToggle}) => {
  return (
    <div className='grid grid-cols-2 bg-headerBg py-3 px-3 fixed w-full z-10'>
     <div className='flex items-center'>
      <button className='mr-3' onClick={()=>handleToggle()}><svg width="17" height="18" viewBox="0 0 24 18" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M0 18H24V15H0V18ZM0 10.5H24V7.5H0V10.5ZM0 0V3H24V0H0Z" fill="white"/>
</svg>
</button>
      <h4 className='text-white'>Sales Dashboard</h4>
     </div>
     <div className='text-white text-right flex items-center justify-end mr-4'>
      <span className='mr-5'>Hello user</span>
      <figure className='w-[45px] h-[45px] rounded-full overflow-hidden'>
        <img src="../images/user.jpg" alt='' className='w-full h-full object-cover'/>
      </figure>
     </div>
    </div>
  )
}

export default Header