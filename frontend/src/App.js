import './App.css';
import './assets/css/style.css'
import { BrowserRouter as Router } from 'react-router-dom';
import RoutesComponent from './components/RoutesComponent';

function App() {
  return (
    <div className="App">
       <Router>
      <RoutesComponent />
    </Router>
    </div>
  );
}

export default App;
