import icon1 from "../assets/images/icon1.png"
import icon2 from "../assets/images/icon2.png"
import icon3 from "../assets/images/icon3.png"
import icon4 from "../assets/images/icon4.png"
import icon5 from "../assets/images/icon5.png"

const sidebarMenu = [
    {
      id: 1,
      menu: "Sales Overview",
      icon: icon1
    },
    {
      id: 2,
      menu: "Stores",
      icon: icon2
    },
    {
      id: 3,
      menu: "Notifications",
      icon: icon3

    },
    {
      id: 4,
      menu: "Settings",
      icon: icon4

    },
    {
      id: 5,
      menu: "Light Theme",
      icon: icon5

    }
  ];
  
  export default sidebarMenu;
  